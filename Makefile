procesador: base.c
	gcc -Wall base.c -lpng -pthread -o procesador

run1:
	./procesador pugcito.png pungcito1h.png -n 1

run2:
	./procesador pugcito.png pungcito2h.png -n 2

run3:
	./procesador pugcito.png pungcito3h.png -n 3

run4:
	./procesador pugcito.png pungcito4h.png -n 4

run5:
	./procesador pugcito.png pungcito5h.png -n 5

run6:
	./procesador pugcito.png pungcito6h.png -n 6

run7:
	./procesador pugcito.png pungcito7h.png -n 7

run8:
	./procesador pugcito.png pungcito8h.png -n 8

.PHONY:clean
clean:
	rm -f procesador
	rm -f pungcito[1-8]h.png
