#include <stdio.h>
#include <png.h>
#include <pthread.h>
#include <stdlib.h>


/* Parte del codigo tomado de https://gist.github.com/niw/5963798 */

#include <stdlib.h>
#include <stdio.h>
#include <png.h>
double tiempo(){
	struct timespec tsp;

	clock_gettime(CLOCK_REALTIME, &tsp);

	double secs = (double)tsp.tv_sec;
	double nano = (double)tsp.tv_nsec / 1000000000.0;

	return secs + nano;
}

///////////////////ABRIR ARCHIVO PNG///////////////////////////////
//Retorna los pixeles de la imagen, y los datos relacionados en los argumentos: ancho, alto, tipo de color (normalmente RGBA) y bits por pixel (usualemente 8 bits)
png_bytep * abrir_archivo_png(char *filename, int *width, int *height, png_byte *color_type, png_byte *bit_depth) {
  FILE *fp = fopen(filename, "rb");

  png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if(!png) abort();

  png_infop info = png_create_info_struct(png);
  if(!info) abort();

  if(setjmp(png_jmpbuf(png))) abort();

  png_init_io(png, fp);

  png_read_info(png, info);

  //resolucion y color de la image. Usaremos espacio de color RGBA
  *width      = png_get_image_width(png, info);
  *height     = png_get_image_height(png, info);
  *color_type = png_get_color_type(png, info);
  *bit_depth  = png_get_bit_depth(png, info);

  // Read any color_type into 8bit depth, RGBA format.
  // See http://www.libpng.org/pub/png/libpng-manual.txt

  if(*bit_depth == 16)
    png_set_strip_16(png);

  if(*color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_palette_to_rgb(png);

  // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
  if(*color_type == PNG_COLOR_TYPE_GRAY && *bit_depth < 8)
    png_set_expand_gray_1_2_4_to_8(png);

  if(png_get_valid(png, info, PNG_INFO_tRNS))
    png_set_tRNS_to_alpha(png);

  // These color_type don't have an alpha channel then fill it with 0xff.
  if(*color_type == PNG_COLOR_TYPE_RGB ||
     *color_type == PNG_COLOR_TYPE_GRAY ||
     *color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

  if(*color_type == PNG_COLOR_TYPE_GRAY ||
     *color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
    png_set_gray_to_rgb(png);

  png_bytep *row_pointers;
  png_read_update_info(png, info);
  row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * (*height));
  if(row_pointers == NULL){
      printf("Error al obtener memoria de la imagen\n");
      exit(-1);
  }
  for(int y = 0; y < *height; y++) {
    row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png,info));
  }

  png_read_image(png, row_pointers);

  fclose(fp);
  return row_pointers;
}
//////////////////////////GUARDAR IMAGEN PNG///////////////////////////////
//Usaremos bit depth 8
//Color type PNG_COLOR_TYPE_GRAY_ALPHA
void guardar_imagen_png(char *filename, int width, int height, png_byte color_type, png_byte bit_depth, png_bytep *res) {
  //int y;
  FILE *fp = fopen(filename, "wb");
  if(!fp) abort();
  
  png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png) abort();

  png_infop info = png_create_info_struct(png);
  if (!info) abort();

  if (setjmp(png_jmpbuf(png))) abort();

  png_init_io(png, fp);

  // Salida es escala de grises
  png_set_IHDR(
    png,
    info,
    width, height,
    bit_depth,
    color_type,
    PNG_INTERLACE_NONE,
    PNG_COMPRESSION_TYPE_DEFAULT,
    PNG_FILTER_TYPE_DEFAULT
  );
  png_write_info(png, info);

  // To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
  // Use png_set_filler().
  //png_set_filler(png, 0, PNG_FILLER_AFTER);
  png_write_image(png, res);	//row_pointers
  png_write_end(png, NULL);
  for(int y = 0; y < height; y++) {
    free(res[y]);		//row_pointers
  }
  free(res);			//row_pointers

  fclose(fp);
}
////////////////////////PROCESAR ARCHIVO PNG/////////////////////////////
typedef struct Datap {
	int width;
	int height;
        int n;
	int n_threads;
	png_bytep* colored_ptrs;
	png_bytep* gray_ptrs;
} Data;
//Retorna el arreglo de pixeles
void* procesar_archivo_png(void* data_p) {
  Data* data= (Data*)data_p;
  //filas de nueva imagen
  for(int y = data->n; y < data->height; y+=data->n_threads) {
    //espacio para la los pixeles de dicha fila
    data->gray_ptrs[y] = malloc(sizeof(png_bytep)*(data->width)*2);			//greyscale con alpha, 2 bytes por pixel
    if(data->gray_ptrs[y] == NULL){
      printf("No se pudo reservar espacio para la imagen procesada");
      exit(-1);
    }
    png_bytep row = data->colored_ptrs[y];
    for(int x = 0; x < data->width; x++) {
      png_bytep px = &(row[x * 4]);
      //Convertimos a escala de grises
      float a = .299f * px[0] + .587f * px[1] + .114f * px[2];
      data->gray_ptrs[y][2*x] = a;
      data->gray_ptrs[y][2*x + 1] = px[3]; //transparencia... dejamos el campo de transparencia igual.
    }
  }
  free(data_p);
  return NULL;
}

//Retorna el arreglo de pixeles
png_bytep* procesar_hilo_png(int width, int height, png_bytep* colored_ptrs, int n_threads) {
	//filas de nueva imagen
	png_bytep* gray_ptr= (png_bytep*)malloc(sizeof(png_bytep *) * height);
	// Create memory area to hold thread pointers.
	pthread_t* thread_ptrs = (pthread_t*) malloc(sizeof(thread_ptrs) * n_threads);
	// Create each thread to process a specific set of rows.
	for (int t = 0; t < n_threads; t++){
		// Thread will be responsible for deleting row_data.
		Data* data          = (Data*) malloc(sizeof(Data));
		data->width            = width;
		data->height           = height;
		data->n_threads        = n_threads;
		data->n                = t;
		data->gray_ptrs        =gray_ptr;
		data->colored_ptrs     = colored_ptrs;
		int err = pthread_create(thread_ptrs +t, NULL, procesar_archivo_png, (void*) data);
		if (err != 0){
			fprintf(stderr, "Error durante la creacion del hilo.");
			exit(EXIT_FAILURE);
		}fprintf(stderr, "Hilo %d ha iniciado.\n", t);
	}
	// Wait for every thread to finish.
	for (int t = 0; t < n_threads; t++){

		void* return_holders = NULL;
		int err              = pthread_join(thread_ptrs[t], &return_holders);

		if (err != 0){
			fprintf(stderr, "Error esperando a union con hilo hijo.\n");
			exit(EXIT_FAILURE);
		}

		fprintf(stderr, "Hilo %d ha terminado.\n", t);
	}
	

	// Deallocate thread memory areas.
	free(thread_ptrs);
	return gray_ptr;
}



int main(int argc, char *argv[]) {
  if(argc != 5) abort();
  int N = atoi(argv[4]);
	
	if (N < 0){
		fprintf(stderr, "\nArgumento -n debe ser mayor a 0\n");
		return -1;
	}

	if (N > 16){
		fprintf(stderr, "\nArgumento -n tiene que ser igual o menor a 16.\n");
		return -1;
	}


  //Datos de la imagen original
  int width, height;
  png_byte color_type; 
  png_byte bit_depth;
  png_bytep *pixeles;
  png_bytep *pixeles_res;
  printf("abrir archivo\n");
  pixeles = abrir_archivo_png(argv[1],&width, &height, &color_type, &bit_depth);
  printf("empezar hilos\n");
  double ini = tiempo();
  pixeles_res = procesar_hilo_png(width, height, pixeles,N);
  double fin = tiempo();
  printf("hilo terminado\n");
  double delta = fin - ini;
  fprintf(stdout,	"%f\n", delta);
  guardar_imagen_png(argv[2], width, height, PNG_COLOR_TYPE_GRAY_ALPHA, bit_depth, pixeles_res);
  printf("imagen guardada\n");
  for(int y = 0; y < height; y++) {
	free(pixeles[y]);
	}
  free(pixeles);

  return 0;
}
